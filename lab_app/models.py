from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models

permissions = [
    (0, 'Read'),
    (1, 'Write')
]


class CustomUser(AbstractUser):
    student = models.BooleanField(default=True)


class UploadImage(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE, related_name='owner')
    title = models.CharField(max_length=400)
    image = models.ImageField(upload_to='images')
    thumbnails = models.ImageField(upload_to='thumbnails', editable=False)
    pub_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Comments(models.Model):
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE,
                              related_name='comment_owner')
    post = models.ForeignKey(UploadImage, on_delete=models.CASCADE)
    comment = models.TextField()
    pub_date = models.DateTimeField(auto_now_add=True)
    visible = models.BooleanField(default=True)

    def __str__(self):
        return self.comment


class SharedPost(models.Model):
    image = models.ForeignKey(UploadImage, blank=True, on_delete=models.CASCADE)
    person = models.ForeignKey(settings.AUTH_USER_MODEL, blank=True, on_delete=models.CASCADE)
    # write_permission = models.BooleanField(default=False)
    permission_levels = models.IntegerField(choices=permissions, default=0)

    def __str__(self):
        return self.image.title
