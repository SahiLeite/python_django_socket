import pdb

from channels.consumer import AsyncConsumer
from channels.db import database_sync_to_async
from kombu.utils import json

from .models import Comments
from .models import UploadImage
from .models import SharedPost


class ChatConsumer(AsyncConsumer):
    async def websocket_connect(self, event):
        print('connected', event)

        post_id = self.scope['url_route']['kwargs']['post_id']

        comments = list(
            Comments.objects.filter(post_id=post_id, visible=True).order_by('-pub_date').values('id', 'owner',
                                                                                                'comment'))

        chat_room = post_id
        self.chat_room = chat_room

        await self.channel_layer.group_add(
            chat_room,
            self.channel_name,
        )

        await self.send({
            'type': 'websocket.accept',
        })

        await self.send({
            'type': 'websocket.send',
            'text': json.dumps(comments)
        })

    async def websocket_receive(self, event):
        print('receive', event)
        post_id = self.scope['url_route']['kwargs']['post_id']
        front_text = event.get('text', None)
        if front_text is not None:
            print(front_text)
            loaded_dic_date = json.loads(front_text)
            type = loaded_dic_date.get('type', None)
            print('type', type)
            writed_comment = loaded_dic_date.get('message', None)
            edited_comment = loaded_dic_date.get('new_comment', None)
            edited_comment_id = loaded_dic_date.get('comment_id', None)
            if type == 'new_comment':
                print('writing new one')
                user = self.scope['user']
                post_id = self.scope['url_route']['kwargs']['post_id']

                try:
                    shared_post = SharedPost.objects.get(image_id=int(post_id))
                except:
                    shared_post = None
                if shared_post and shared_post.person == user and shared_post.permission_levels == 1:
                    comment_id = await self.create_comment_object(writed_comment, user, post_id)
                else:
                    raise Exception('You do not have permission to write comment!')

                my_response = {
                    'id': comment_id,
                    'comment': writed_comment,
                    'owner': user.id
                }

                await self.channel_layer.group_send(
                    self.chat_room,
                    {
                        'type': 'chat_message',
                        'text': json.dumps(my_response)
                    }
                )

            elif type == 'editing_comment':
                print('editing')
                edited_comment_obj = Comments.objects.get(id=edited_comment_id)
                edited_comment_obj.comment = edited_comment
                edited_comment_obj.save()

                comments = list(
                    Comments.objects.filter(post_id=post_id, visible=True).order_by('-pub_date').values('id', 'owner',
                                                                                                        'comment'))

                await self.channel_layer.group_send(
                    self.chat_room,
                    {
                        'type': 'chat_message',
                        'text': json.dumps(comments)
                    }
                )

            elif type == 'deleting_comment':
                print('deleting')
                edited_comment_obj = Comments.objects.get(id=edited_comment_id)
                edited_comment_obj.visible = False
                edited_comment_obj.save()

                comments = list(
                    Comments.objects.filter(post_id=post_id, visible=True).order_by('-pub_date').values('id', 'owner',
                                                                                                        'comment'))

                await self.channel_layer.group_send(
                    self.chat_room,
                    {
                        'type': 'chat_message',
                        'text': json.dumps(comments)
                    }
                )

    async def chat_message(self, event):
        await self.send({
            'type': 'websocket.send',
            'text': event['text']
        })

    async def websocket_disconnect(self, event):
        print('disconnect', event)

    @database_sync_to_async
    def create_comment_object(self, comment, user, post_id):
        post = UploadImage.objects.get(pk=post_id)
        new_comment = Comments.objects.create(owner=user, post=post, comment=comment)
        return new_comment.id
