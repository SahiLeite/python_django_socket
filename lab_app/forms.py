from django import forms
from django.contrib.auth import get_user_model

from .models import UploadImage

User = get_user_model()


class UserForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)
    confirm_password = forms.CharField(label='Confirm Password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username']

    def clean_username(self, *args, **kwargs):
        username = self.cleaned_data.get('username')

        user = User.objects.filter(username=username)
        if user.exists():
            raise forms.ValidationError("This email already in use!")
        return username

    def clean_confirm_password(self):
        password1 = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('confirm_password')

        if not len(password1) > 4:
            raise forms.ValidationError('less than 5')

        if not password1 == password2:
            raise forms.ValidationError('Passwords must match')
        return password1


class UploadImageForm(forms.ModelForm):
    class Meta:
        model = UploadImage
        fields = ['title', 'image']
