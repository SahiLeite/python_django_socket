import os
import pdb

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import login, authenticate, logout, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.http import JsonResponse
from django.shortcuts import render, redirect
from django.views import View

from .forms import UploadImageForm
from .forms import UserForm
from .models import SharedPost
from .models import UploadImage
from .tasks import resize_image_task

User = get_user_model()


class IndexView(LoginRequiredMixin, View):
    template_name = 'index/index.html'
    login_url = '/login/'
    redirect_field_name = ''

    def get(self, request):
        return render(request, self.template_name)


class RegisterView(View):
    template_name = 'auth/register.html'

    def get(self, request):
        context = {}
        context['form'] = UserForm(None)

        if request.user.is_authenticated:
            return redirect('/')
        return render(request, self.template_name, context)

    def post(self, request):
        form = UserForm(request.POST)
        if form.is_valid():
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            username = request.POST['username']
            password = request.POST['password']
            user = User.objects.create_user(first_name=first_name, last_name=last_name, username=username,
                                            password=password)
            login(request, user)
            messages.success(request, 'You successfully registered and logged in!')
            return redirect('/')
        return render(request, self.template_name, {'form': form})


class LoginView(View):
    template_name = 'auth/login.html'

    def get(self, request):
        if request.user.is_authenticated:
            return redirect('/')
        return render(request, self.template_name)

    def post(self, request):
        email = request.POST.get('email', None)
        password = request.POST.get('password', None)

        user = authenticate(request, username=email, password=password)

        if user:
            login(request, user)
            messages.success(
                request, "You successfully logged in!"
            )
            return redirect('/')
        messages.error(
            request, "Username and password does not match!"
        )
        return render(request, self.template_name)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('/login')


class UploadImageView(LoginRequiredMixin, View):
    template_name = 'upload_image/upload_image.html'
    login_url = '/login/'
    redirect_field_name = ''

    def get(self, request):
        context = {}
        context['form'] = UploadImageForm(None)
        return render(request, self.template_name, context)

    def post(self, request):
        form = UploadImageForm(request.POST, request.FILES)
        if form.is_valid():
            image = request.FILES.get('image')
            pending_object = form.save()
            image_path = os.path.join(settings.MEDIA_ROOT, 'images/' + image.name.replace(' ', '_'))
            user_id = request.user.id
            thumb_path = 'thumb_' + image.name
            thumb_image_path = os.path.join(settings.MEDIA_ROOT, 'thumbnails/' + thumb_path)
            resize_image_task.delay(image_path, thumb_image_path, pending_object.pk, user_id)

            messages.success(request, 'File successfully uploaded!')
            return redirect('/')
        return render(request, self.template_name, {'form': form})


class YourImagesView(LoginRequiredMixin, View):
    template_name = 'your_images/your_images.html'
    login_url = '/login/'
    redirect_field_name = ''

    def get(self, request):
        user = request.user
        images = UploadImage.objects.filter(user_id=user.id)
        args = {
            'images': images
        }
        return render(request, self.template_name, args)

    def post(self, request):
        post_id = request.POST.get('post_id')
        username = request.POST.get('username')
        permission = request.POST.get('permission')
        permission = 1 if int(permission) == 1 else 0
        post_id = int(post_id)

        try:
            user = User.objects.get(username=username)
        except ObjectDoesNotExist:
            user = None
        try:
            possible_shared_post = SharedPost.objects.filter(image_id=post_id).get(person=user)
        except:
            possible_shared_post = None

        if user:
            post = UploadImage.objects.get(pk=post_id)
            if post.user.id != user.id and not possible_shared_post:
                SharedPost.objects.create(image=post, person=user, permission_levels=permission)
                return JsonResponse({"message": "Successfully shared!!!"})
            elif post.user.id != user.id and possible_shared_post:
                return JsonResponse({"message": "You have shared this post already with {}!!!".format(username)})
            else:
                return JsonResponse({"message": "You cannot share post with yourself!!!"})
        return JsonResponse({"message": "User with this username not found!"})


class ImageDetailsView(LoginRequiredMixin, View):
    template_name = 'your_images/details.html'
    login_url = '/login/'
    redirect_field_name = ''

    def get(self, request, post_id):
        user = request.user.id
        query = Q(user=user) | Q(sharedpost__person=user)
        try:
            post = UploadImage.objects.filter(query).get(pk=post_id)
        except:
            raise Exception('You do not have access to this post')
        try:
            post_permission = SharedPost.objects.get(image_id=post_id).permission_levels
        except:
            post_permission = None

        args = {
            'post': post,
            'permission': post_permission,
            'user': user,
        }
        return render(request, self.template_name, args)


class SharedWithYouView(LoginRequiredMixin, View):
    template_name = 'shared_images/shared_with_you.html'
    login_url = '/login/'
    redirect_field_name = ''

    def get(self, request):
        user = request.user
        shared_posts = SharedPost.objects.filter(person=user)

        args = {
            'shared_posts': shared_posts
        }
        return render(request, self.template_name, args)
