import os
from datetime import datetime

from PIL import Image
from celery import shared_task
from celery.utils.log import get_task_logger
from django.contrib.auth import get_user_model
from django.core.files import File

from .models import UploadImage

logger = get_task_logger(__name__)
User = get_user_model()


@shared_task
def resize_image_task(image_path, thumb_image_path, id, user_id):
    logger.info('started resize_image_task function at time {0}'.format(datetime.now()))
    pending_object = UploadImage.objects.get(pk=id)
    image = Image.open(image_path)
    image = image.resize((200, 200), Image.ANTIALIAS)
    image.save(thumb_image_path)
    pending_object.thumbnails = File(open(thumb_image_path, 'rb'))
    pending_object.thumbnails.name = pending_object.thumbnails.name[23:]  # 60  in local pwd + 1 general
    pending_object.user = User.objects.get(pk=user_id)
    pending_object.save()

    try:
        os.remove(thumb_image_path)
    except:
        pass
